package TCPExercises;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * TCP TCPExercises used to send message to localhost server. Message is taken from standard input
 */
public class TcpClient implements Runnable {

    public static void main(String[] args) {
        TcpClient client = new TcpClient();
        client.run();
    }

    @Override
    public void run() {
        try (Socket clientSocket = new Socket(InetAddress.getLocalHost(), 7)) {
            System.out.println("Connected to server.");
            sendClientData(clientSocket);
            receiveServerData(clientSocket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendClientData(Socket socket) {
        System.out.println("Enter message:");
        try (PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true)) {
            Scanner sc = new Scanner(System.in);
            String line;
            do {
                line = sc.nextLine();
                printWriter.println(line);
            } while (!"quit".equalsIgnoreCase(line) && !"q".equalsIgnoreCase(line));
        } catch (IOException e) {
            if (socket.isClosed()) {
                Logger.getGlobal().log(Level.SEVERE, "Cannot send data stream. Socket may be closed.");
            }
            throw new RuntimeException("Cannot send data stream to server", e);
        }
    }

    private void receiveServerData(Socket socket) {
        System.out.println("FROM SERVER: ");
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
            while (!bufferedReader.ready()) {
                // WAIT
            }
            bufferedReader.lines().forEach(System.out::println);
        } catch (IOException e) {
            if (socket.isClosed()) {
                Logger.getGlobal().log(Level.SEVERE, "Cannot read input stream. Socket may be closed.");
            } else {
                throw new RuntimeException("Cannot read from socket input stream", e);
            }
        }
    }
}