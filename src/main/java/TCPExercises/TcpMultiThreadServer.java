package TCPExercises;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.stream.Collectors;

public class TcpMultiThreadServer extends TcpAbstractServer {

    private int port;

    public TcpMultiThreadServer() {
        this.port = DEFAULT_SERVER_PORT;
    }

    public TcpMultiThreadServer(int port) {
        this.port = port;
    }

    public static void main(String[] args) {
        Thread serverThread = new Thread(new TcpMultiThreadServer(), "TCP Multi-thread server");
        serverThread.start();
    }

    @Override
    public void run() {

    }

    protected Socket accept(ServerSocket serverSocket) {
        try {
            return serverSocket.accept();
        } catch (IOException e) {
            throw new RuntimeException("Cannot instatata");
        }
    }

    @Override
    protected Socket accept() {
        return null;
    }
}

class ClientThread implements Runnable {

    protected Socket socket;
    protected String serverText;

    ClientThread(Socket clientSocket, String serverText) {
        this.socket = clientSocket;
        this.serverText = serverText;
    }

    public void run() {
        String clientMessage = waitForClientMessage(socket);
        try (PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true)) {
            while (!socket.isClosed()) {
                System.out.println(String.format("Client %s sent message: ", socket.getInetAddress()));
                System.out.println(clientMessage);
                printWriter.println("Message received");
            }
        } catch (IOException e) {
            throw new RuntimeException("Error during printing message");
        }
    }

    private String waitForClientMessage(Socket socket) {
        try (BufferedReader messageFromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
            while (!messageFromClient.ready()) {
                // WAIT
            }
            System.out.println("Message received: ");
            return messageFromClient.lines().collect(Collectors.joining());
        } catch (IOException e) {
            throw new RuntimeException("Cannot receive message");
        }
    }
}
