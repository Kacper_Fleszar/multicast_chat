package TCPExercises;

import java.io.IOException;
import java.net.Socket;
import java.util.List;

/**
 * TCP server used to get message from TCPExercises.
 */
public class TcpServer extends TcpAbstractServer {

    public TcpServer() {
        new TcpServer(DEFAULT_SERVER_PORT);
    }

    public TcpServer(int port) {
        this.port = port;
    }

    public static void main(String[] args) {
        TcpServer server = new TcpServer();
        server.run();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Stopping server");
        server.stop();
    }

    @Override
    public void run() {
        startServerSocket();
        while (!isClosed()) {
            Socket socket = accept();
            List<String> clientStream = readClientStream(socket);
            sendResponse(socket, clientStream);
//            } catch (IOException e) {
//                if (isClosed()) {
//                    System.out.println("Server stopped.");
//                    return;
//                }
//                throw new RuntimeException("Error accepting TCPExercises connection", e);
//            }
        }
    }

    protected Socket accept() {
        try {
            Socket socket = serverSocket.accept();
            System.out.println(String.format("Client %s connected", socket.getInetAddress()));
            return socket;
        } catch (IOException e) {
            throw new RuntimeException("Cannot accept socket on ");
        }
    }
}
