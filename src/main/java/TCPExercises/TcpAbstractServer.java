package TCPExercises;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public abstract class TcpAbstractServer implements Runnable {

    static final int DEFAULT_SERVER_PORT = 7;

    ServerSocket serverSocket;
    int port = DEFAULT_SERVER_PORT;

    protected void startServerSocket() {
        try {
            serverSocket = new ServerSocket(port);
            System.out.println(String.format("Starting TCP server listening on port %s", serverSocket.getLocalPort()));
        } catch (IOException e) {
            throw new RuntimeException("Cannot open server socket", e);
        }
    }

    protected abstract Socket accept();

    protected List<String> readClientStream(Socket socket) {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
            bufferedReader.lines().forEach(System.out::println);
            return bufferedReader.lines().collect(Collectors.toList());
        } catch (IOException | UncheckedIOException e) {
            if (socket.isClosed()) {
                Logger.getGlobal().log(Level.SEVERE, "Cannot read input stream. Socket may be closed.");
            } else {
                throw new RuntimeException("Cannot read from socket input stream", e);
            }
        }
        return null;
    }

    protected void sendResponse(Socket socket, List<String> response) {
        try (PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true)) {
            response.forEach(str -> printWriter.println(str));
            printWriter.flush();
        } catch (IOException e) {

        }
    }

    protected void stop() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            throw new RuntimeException("Cannot close server socket", e);
        }
    }

    protected boolean isClosed() {
        return serverSocket.isClosed();
    }


}
