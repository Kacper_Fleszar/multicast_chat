package chat.sender;

import chat.utils.ChatProperties;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import static chat.utils.Log.printDebug;

public class ChatSender {

    private static int port;

    /**
     * Multi-cast message to multi-cast group specified in {@link ChatProperties}
     *
     * @param message message to send
     */
    public static void multicast(String message) {
        if (message == null || message.isEmpty()) {
            return;
        }
        try (DatagramSocket multicastSocket = startSender()) {
            multicast(multicastSocket, message);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static int getPort() {
        return port;
    }

    private static void multicast(DatagramSocket socket, String msg) throws IOException {
        printDebug("Sending: " + msg);
        byte[] sendBuffer = msg.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length,
                ChatProperties.MULTICAST_GROUP_INET_ADDRESS, ChatProperties.CHAT_MULTICAST_GROUP_PORT);
        socket.send(sendPacket);
    }

    private static DatagramSocket startSender() throws IOException {
        DatagramSocket socket;
        if (port == 0) {
            socket = new DatagramSocket();
            port = socket.getLocalPort();
        } else {
            socket = new DatagramSocket(port);
        }
        printDebug(String.format("Started sender on port: %s", port));
        return socket;
    }


}
