package chat;

import chat.receiver.ChatReceiver;

public class RunChat {

    public static void main(String[] args) throws InterruptedException {
        ChatUser user = ChatUser.getInstance();
        ChatReceiver receiver = new ChatReceiver();
        new Thread(receiver).start();
        Thread.sleep(1000);
        user.registerNickname();
        user.chat();
    }
}
