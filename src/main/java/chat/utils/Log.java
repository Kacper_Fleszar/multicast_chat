package chat.utils;

import java.time.LocalTime;

public class Log {

    public static final void printDebug(String log) {
        if (!ChatProperties.IS_DEBUG) {
            return;
        }
        System.out.println(LocalTime.now() + " [DEBUG] " + log);
    }
}
