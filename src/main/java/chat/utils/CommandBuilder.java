package chat.utils;

/**
 * TODO: link this with chat patterns class
 * TODO: get {@link chat.ChatUser} as parameter
 */
public class CommandBuilder {

    /**
     * Build command for registering nickname
     *
     * @param nickname User nickname
     * @return Registering command (i.e. NICK user)
     */
    public static String buildNickRegisterCommand(String nickname) {
        return String.format("NICK %s", nickname);
    }

    /**
     * Build response when nickname is taken by another user
     *
     * @param nickname Sending user nickname
     * @return Busy command (i.e. NICK user BUSY)
     */
    public static String buildBusyNickResponse(String nickname) {
        return String.format("NICK %s BUSY", nickname);
    }

    /**
     * TODO: javadoc
     * @param room room name
     * @param nickname nick name
     * @return Join room command (i.e. JOIN room user)
     */
    public static String buildJoinRoomCommand(String room, String nickname) {
        return String.format("JOIN %s %s", room, nickname);
    }

    /**
     * TODO: javadoc
     * @param room room name
     * @param nickname nick name
     * @return Leave room command (i.e. LEFT room user)
     */
    public static String buildLeaveRoomCommand(String room, String nickname) {
        return String.format("LEFT %s %s", room, nickname);
    }

    /**
     * Build command used to sending messages
     *
     * @param nickname nickname
     * @param room roomname
     * @param msg message to send
     * @return Command to send (i.e. MSG user room lorem ipsum)
     */
    public static String buildMessage(String nickname, String room, String msg) {
        return String.format("MSG %s %s %s", nickname, room, msg);
    }
}
