package chat.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class ChatProperties {

    /**
     * Multi-cast host, must be between 224.0.0.0 and 239.255.255.255
     */
    public static final InetAddress MULTICAST_GROUP_INET_ADDRESS;

    /**
     * Multi-cast group port for receiving
     */
    public static final int CHAT_MULTICAST_GROUP_PORT = 3307;

    /**
     * Default buffer size
     */
    public static final int BUFFER_SIZE = 1024;

    /**
     * Socket timeout in milliseconds
     */
    public static final int SOCKET_TIMEOUT = 1000;

    /**
     * Enable debug logging, such as sender and receiver thread logs
     */
    static final boolean IS_DEBUG = false;

    static {
        try {
            MULTICAST_GROUP_INET_ADDRESS = InetAddress.getByName("230.0.0.255");
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }
}


