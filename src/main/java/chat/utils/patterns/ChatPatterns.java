package chat.utils.patterns;

public class ChatPatterns {

    /**
     * Pattern for match: NICK nickname <br/>
     * Pattern must have NICK and nickname without whitespaces, but can have zero or more whitespaces;
     */
    public static final NickNamePattern NICK_NAME_PATTERN = NickNamePattern.compile();

    /**
     * Pattern for match: NICK nickname BUSY <br/>
     * Pattern must have NICK, nickname without whitespaces, and BUSY, but can have zero or more whitespaces
     * in the end of the command;
     */
    public static final NickNameBusyPattern NICK_NAME_BUSY_PATTERN = NickNameBusyPattern.compile();

    /**
     * Pattern for match: NICK room nick <br/>
     * Pattern must have NICK, nickname without whitespaces and room without whitespaces, but can have zero or more whitespaces
     * in the end of the command;
     */
    public static final JoinRoomNickPattern JOIN_ROOM_NICK_PATTERN = JoinRoomNickPattern.compile();

    /**
     * TODO: add java-doc
     */
    public static final JoinRoomPattern JOIN_ROOM_PATTERN = JoinRoomPattern.compile();

    /**
     * TODO: add java-doc
     */
    public static final LeavePattern LEAVE_PATTERN = LeavePattern.compile();

    /**
     * TODO: add java-doc
     */
    public static final LeftRoomNickPattern LEFT_ROOM_NICK_PATTERN = LeftRoomNickPattern.compile();

    /**
     * Pattern for match: MSG nickname room message <br/>
     * Pattern must have MSG, room name, nickname without whitespaces, and rest of command is message to send
     */
    public static final MsgNickRoomMessagePattern MSG_NICK_ROOM_MESSAGE_PATTERN = MsgNickRoomMessagePattern.compile();
}
