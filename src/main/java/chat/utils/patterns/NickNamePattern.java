package chat.utils.patterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NickNamePattern {

    private static final String REGEX = "NICK (\\w+)\\s*$";

    private Pattern pattern;

    private NickNamePattern() {}

    public static NickNamePattern compile() {
        NickNamePattern instance = new NickNamePattern();
        instance.pattern = Pattern.compile(REGEX);
        return instance;
    }

    public boolean matches(String s) {
        return pattern.matcher(s).matches();
    }

    public String getNickname(String s) {
        Matcher matcher = pattern.matcher(s);
        if (matcher.matches()) {
            return matcher.group(1);
        }
        return "";
    }
}
