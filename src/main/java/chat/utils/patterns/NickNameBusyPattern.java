package chat.utils.patterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NickNameBusyPattern {

    private static final String REGEX = "NICK (\\w+) BUSY\\s*";

    private Pattern pattern;

    private NickNameBusyPattern() {}

    public static NickNameBusyPattern compile() {
        NickNameBusyPattern instance = new NickNameBusyPattern();
        instance.pattern = Pattern.compile(REGEX);
        return instance;
    }

    public boolean matches(String s) {
        return pattern.matcher(s).matches();
    }

    public String getNickname(String s) {
        Matcher matcher = pattern.matcher(s);
        if (matcher.matches()) {
            return matcher.group(1);
        }
        return "";
    }
}
