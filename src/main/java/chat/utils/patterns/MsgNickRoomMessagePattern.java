package chat.utils.patterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MsgNickRoomMessagePattern {

    private static final String REGEX = "MSG (\\w+) (\\w+) (.*)";

    private Pattern pattern;

    private MsgNickRoomMessagePattern() {}

    static MsgNickRoomMessagePattern compile() {
        MsgNickRoomMessagePattern instance = new MsgNickRoomMessagePattern();
        instance.pattern = Pattern.compile(REGEX);
        return instance;
    }

    public boolean matches(String s) {
        return pattern.matcher(s).matches();
    }

    public String getNickname(String s) {
        Matcher matcher = pattern.matcher(s);
        if (matcher.matches()) {
            return matcher.group(1);
        }
        return "";
    }

    public String getRoom(String s) {
        Matcher matcher = pattern.matcher(s);
        if (matcher.matches()) {
            return matcher.group(2);
        }
        return "";
    }

    public String getMessage(String s) {
        Matcher matcher = pattern.matcher(s);
        if (matcher.matches()) {
            return matcher.group(3);
        }
        return "";
    }
}
