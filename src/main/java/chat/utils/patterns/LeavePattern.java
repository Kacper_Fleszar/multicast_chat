package chat.utils.patterns;

import java.util.regex.Pattern;

public class LeavePattern {

    private static final String REGEX = "LEAVE\\s*";

    private Pattern pattern;

    private LeavePattern() {}

    static LeavePattern compile() {
        LeavePattern instance = new LeavePattern();
        instance.pattern = Pattern.compile(REGEX);
        return instance;
    }

    public boolean matches(String s) {
        return pattern.matcher(s).matches();
    }
}
