package chat.utils.patterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JoinRoomNickPattern {

    private static final String REGEX = "JOIN (\\w+) (\\w+)\\s*";

    private Pattern pattern;

    private JoinRoomNickPattern() {}

    static JoinRoomNickPattern compile() {
        JoinRoomNickPattern instance = new JoinRoomNickPattern();
        instance.pattern = Pattern.compile(REGEX);
        return instance;
    }

    public boolean matches(String s) {
        return pattern.matcher(s).matches();
    }

    public String getNickname(String s) {
        Matcher matcher = pattern.matcher(s);
        if (matcher.matches()) {
            return matcher.group(2);
        }
        return "";
    }

    public String getRoom(String s) {
        Matcher matcher = pattern.matcher(s);
        if (matcher.matches()) {
            return matcher.group(1);
        }
        return "";
    }
}
