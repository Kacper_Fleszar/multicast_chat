package chat.utils.patterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JoinRoomPattern {

    private static final String REGEX = "JOIN (\\w+)\\s*";

    private Pattern pattern;

    private JoinRoomPattern() {}

    static JoinRoomPattern compile() {
        JoinRoomPattern instance = new JoinRoomPattern();
        instance.pattern = Pattern.compile(REGEX);
        return instance;
    }

    public boolean matches(String s) {
        return pattern.matcher(s).matches();
    }

    public String getRoom(String s) {
        Matcher matcher = pattern.matcher(s);
        if (matcher.matches()) {
            return matcher.group(1);
        }
        return "";
    }
}
