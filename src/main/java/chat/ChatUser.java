package chat;

import chat.sender.ChatSender;
import chat.utils.patterns.ChatPatterns;
import chat.utils.CommandBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static chat.utils.Log.printDebug;

public class ChatUser {

    private static ChatUser instance;
    private String userNickname = "";
    private String registerNicknameResponse = "";
    private String room = "";
    private boolean isRegistered = false;

    private ChatUser() {
    }

    public static ChatUser getInstance() {
        if (instance == null) {
            instance = new ChatUser();
        }
        return instance;
    }

    public String getUserNickname() {
        return userNickname;
    }

    public String getRoom() {
        return room;
    }

    public void setRegisterNicknameResponse(String registerNicknameResponse) {
        this.registerNicknameResponse = registerNicknameResponse;
    }

    public void printCommandPrompt() {
        String printRoom = room.isEmpty() ? "" : "(" + room.toUpperCase() + ") ";
        System.out.format("%n%s%s > ", printRoom, userNickname);
    }

    void registerNickname() throws InterruptedException {
        while (!isRegistered) {
            userNickname = enterNickname();
            if (userNickname.isEmpty()) {
                continue;
            }
            String nicknameCmd = CommandBuilder.buildNickRegisterCommand(userNickname);
            ChatSender.multicast(nicknameCmd);
            Thread.sleep(500);
            isRegistered = true;
            if (ChatPatterns.NICK_NAME_BUSY_PATTERN.matches(registerNicknameResponse)) {
                System.out.format("Nickname %s is busy%n", userNickname);
                printDebug(String.format("User %s is not registered successfully", userNickname));
                isRegistered = false;
            } else {
                System.out.println("Welcome to the chat " + userNickname);
                printDebug(String.format("User %s registered successfully", userNickname));
            }
        }
    }

    void chat() {
        // TODO: loop end condition
        while (true) {
            if (room.isEmpty()) {
                System.out.println("Please join to room by entering: JOIN <roomname>");
            }
            String text = enterText();
            if (ChatPatterns.JOIN_ROOM_PATTERN.matches(text)) {
                this.room = ChatPatterns.JOIN_ROOM_PATTERN.getRoom(text);
                ChatSender.multicast(CommandBuilder.buildJoinRoomCommand(room, userNickname));
                System.out.println("You can leave room by entering: LEAVE");
            } else if (ChatPatterns.LEAVE_PATTERN.matches(text)) {
                ChatSender.multicast(CommandBuilder.buildLeaveRoomCommand(room, userNickname));
                this.room = "";
            } else {
                String sendMessage = CommandBuilder.buildMessage(userNickname, room, text);
                ChatSender.multicast(sendMessage);
            }
        }
    }

    private String enterNickname() {
        System.out.print("Enter your nickname: ");
        String enteredText = enterText();
        return enteredText.split(" ")[0];
    }

    private String enterText() {
        printCommandPrompt();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new InputStreamReader(System.in));
            return reader.readLine();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
