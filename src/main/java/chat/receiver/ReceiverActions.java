package chat.receiver;

import chat.ChatUser;
import chat.sender.ChatSender;
import chat.utils.CommandBuilder;
import chat.utils.patterns.ChatPatterns;

import java.util.function.BiConsumer;

public enum ReceiverActions {

    IF_NAME_REGISTER(registerNickname()),
    IF_JOIN(joinToGroup()),
    IF_LEAVE(leaveGroup()),
    IF_MESSAGE(printMessage()),
    NO_ACTION(noAction());

    private BiConsumer<ChatUser, String> consumer;

    ReceiverActions(BiConsumer<ChatUser, String> consumer) {
        this.consumer = consumer;
    }

    public void accept(ChatUser user, String receivedString) {
        this.consumer.accept(user, receivedString);
    }

    private static BiConsumer<ChatUser, String> registerNickname() {
        return (user, receivedString) -> {
            String nickname = ChatPatterns.NICK_NAME_PATTERN.getNickname(receivedString);
            if (user.getUserNickname().equals(nickname)) {
                ChatSender.multicast(CommandBuilder.buildBusyNickResponse(nickname));
            }
        };
    }

    private static BiConsumer<ChatUser, String> joinToGroup() {
        return (user, receivedString) -> {
            String room = ChatPatterns.JOIN_ROOM_NICK_PATTERN.getRoom(receivedString);
            if (user.getRoom().equals(room)) {
                System.out.format("%nUser %s has joined to your group", ChatPatterns.JOIN_ROOM_NICK_PATTERN.getNickname(receivedString));
                user.printCommandPrompt();
            }
        };
    }

    private static BiConsumer<ChatUser, String> leaveGroup() {
        return (user, receivedString) -> {
            String room = ChatPatterns.LEFT_ROOM_NICK_PATTERN.getRoom(receivedString);
            if (user.getRoom().equals(room)) {
                System.out.format("%nUser %s left your group", ChatPatterns.LEFT_ROOM_NICK_PATTERN.getNickname(receivedString));
                user.printCommandPrompt();
            }
        };
    }

    private static BiConsumer<ChatUser, String> printMessage() {
        return (user, receivedString) -> {
            String room = ChatPatterns.MSG_NICK_ROOM_MESSAGE_PATTERN.getRoom(receivedString);
            if (user.getRoom().equals(room)) {
                System.out.format("%n(%s) %s: %s", room.toUpperCase(),
                        ChatPatterns.MSG_NICK_ROOM_MESSAGE_PATTERN.getNickname(receivedString),
                        ChatPatterns.MSG_NICK_ROOM_MESSAGE_PATTERN.getMessage(receivedString));
                user.printCommandPrompt();
            }
        };
    }

    private static BiConsumer<ChatUser, String> noAction() {
        return (user, receivedString) -> {};
    }

}