package chat.receiver;

import chat.sender.ChatSender;
import chat.utils.ChatProperties;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;

import static chat.utils.Log.printDebug;

public abstract class ChatThread implements Runnable {

    public abstract void run();

    MulticastSocket startReceiver() throws IOException {
        MulticastSocket socket = new MulticastSocket(ChatProperties.CHAT_MULTICAST_GROUP_PORT);
        socket.setReuseAddress(true);
        socket.setSoTimeout(ChatProperties.SOCKET_TIMEOUT);
        socket.joinGroup(ChatProperties.MULTICAST_GROUP_INET_ADDRESS);

        printDebug(String.format("Started receiver on port: %s", socket.getLocalPort()));
        printDebug(String.format("Receiver joined to multicast group on host %s", ChatProperties.MULTICAST_GROUP_INET_ADDRESS));

        return socket;
    }

    String receive(MulticastSocket socket, long timeout) throws IOException {
        byte[] receiveBuffer = new byte[ChatProperties.BUFFER_SIZE];
        DatagramPacket receivedPacket = new DatagramPacket(receiveBuffer, ChatProperties.BUFFER_SIZE);
        socket.setSoTimeout((int) timeout);
        socket.receive(receivedPacket);
        String receivedString = new String(receivedPacket.getData(), 0, receivedPacket.getLength());
        if (ChatSender.getPort() != receivedPacket.getPort()) {
            printDebug(String.format("Received: '%s' from port %s", receivedString, receivedPacket.getPort()));
            return receivedString;
        }
        // If I get message from myself, show nothing
        return "";
    }
}
