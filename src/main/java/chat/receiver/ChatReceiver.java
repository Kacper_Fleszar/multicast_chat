package chat.receiver;

import chat.ChatUser;
import chat.utils.patterns.ChatPatterns;

import java.io.IOException;
import java.net.MulticastSocket;
import java.net.SocketTimeoutException;
import java.time.Duration;

public class ChatReceiver extends ChatThread {

    private ChatUser chatUser;

    public ChatReceiver() {
        chatUser = ChatUser.getInstance();
    }

    @Override
    public void run() {
        try (MulticastSocket receiverSocket = startReceiver()) {
            while (true) {
                String receivedString;
                try {
                    receivedString = receive(receiverSocket, Duration.ofSeconds(5).toMillis());
                    chatUser.setRegisterNicknameResponse(receivedString);
                } catch (SocketTimeoutException e) {
                    continue;
                }
                getActionByReceivedString(receivedString).accept(chatUser, receivedString);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            System.err.println("RECEIVER CLOSED");
        }
    }

    private ReceiverActions getActionByReceivedString(String receivedString) {
        if (ChatPatterns.NICK_NAME_PATTERN.matches(receivedString)) {
            return ReceiverActions.IF_NAME_REGISTER;
        } else if (ChatPatterns.JOIN_ROOM_NICK_PATTERN.matches(receivedString)) {
            return ReceiverActions.IF_JOIN;
        } else if (ChatPatterns.LEFT_ROOM_NICK_PATTERN.matches(receivedString)) {
            return ReceiverActions.IF_LEAVE;
        } else if (ChatPatterns.MSG_NICK_ROOM_MESSAGE_PATTERN.matches(receivedString)) {
            return ReceiverActions.IF_MESSAGE;
        } else {
            return ReceiverActions.NO_ACTION;
            // TODO: if END; close receiver
        }
    }
}
