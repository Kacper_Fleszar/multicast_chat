# Chat

## Overview

This is University project from Web Developing course. I've created chat based on multicast and UDP protocol.

## Run

To run application just use simple terminal or any IDE with JRE installed. Minimal required version is Java 8.
Main method can be found in RunChat class

## Functionality
This is first part of project. You can only register yourself by nickname and send messages. There is no turn of option